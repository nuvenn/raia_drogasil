(() => {
  const selector = selector => document.querySelector(selector);
  const create = element => document.createElement(element);

  const app = selector("#app");

  const Login = create("div");
  Login.classList.add("login");

  const Logo = create("img");
  Logo.src = "./assets/images/logo.svg";
  Logo.classList.add("logo");

  const Form = create("form");

  Form.onsubmit = async e => {
    e.preventDefault();
    const [email, password] = e.target.parentElement.children[0];
    if (email.value && password.value) {
      const { url } = await fakeAuthenticate(email.value, password.value);
      location.href = "#users";
      const users = await getDevelopersList(url);
      renderPageUsers(users);
    }
  };

  Form.oninput = e => {
    const [email, password, button] = e.target.parentElement.children;
    !email.validity.valid || !email.value || password.value.length <= 5
      ? button.setAttribute("disabled", "disabled")
      : button.removeAttribute("disabled");
    email.value && password.value
      ? button.classList.add("login__button--filled")
      : button.classList.remove("login__button--filled");
  };

  Form.innerHTML =
    "<div class='login__form'>" +
    "<input type='email' name='email' placeholder='Entre com seu e-mail' class='login__input' required></input>" +
    "<input type='password' name='password' placeholder='Digite sua senha supersecreta' class='login__input' required></input>" +
    "<button class='login__button'>Entrar</button>" +
    "</div>";

  app.appendChild(Logo);
  Login.appendChild(Form);
  app.appendChild(Login);

  async function fakeAuthenticate(email, password) {
    const res = await fetch("http://www.mocky.io/v2/5dba690e3000008c00028eb6");
    const data = await res.json();

    const fakeJwtToken = `${btoa(email + password)}.${btoa(
      data.url
    )}.${new Date().getTime() + 300000}`;

    localStorage.setItem("token", fakeJwtToken);
    return data;
  }

  async function getDevelopersList(url) {
    const res = await fetch(url);
    const data = await res.json();
    return data;
  }

  function renderPageUsers(users) {
    app.classList.add("logged");
    Login.style.display = "none";
    Logo.classList.add("logo--users");

    const Ul = create("ul");
    Ul.classList.add("container");

    users.forEach(element => {
      const Li = create("li");
      const Profile = create("img");
      Profile.src = element.avatar_url;
      Li.innerHTML = element.login;
      Li.appendChild(Profile);
      Ul.appendChild(Li);
    });

    app.appendChild(Ul);
  }

  // init
  (async function() {
    const rawToken = null;
    const token = rawToken ? rawToken.split(".") : null;
    if (!token || token[2] < new Date().getTime()) {
      localStorage.removeItem("token");
      location.href = "#login";
      app.appendChild(Login);
    } else {
      location.href = "#users";
      const users = await getDevelopersList(atob(token[1]));
      renderPageUsers(users);
    }
  })();
})();
